# Tomcat SSL Auto Renewal

This Python script uses [Watchdog](https://pythonhosted.org/watchdog/) to monitor the [Let's Encrypt](https://letsencrypt.org/) certificate files stored in `/etc/letsencrypt/live/<your-domain.com>/` and copy them into `/opt/tomcat/conf/` each time they're renewed.

The installer was created by [Guillermo Castellano](https://guillearch.me).

The code is licensed under the [MIT License](https://gitlab.com/guillearch/tomcat-ssl-auto-renewal/blob/master/LICENSE). Feel free to adapt the script for your needs!

## Requirements

* GNU/Linux.
* Python 3.6 or higher.
* Tomcat.
* Let's Encrypt certificate installed and a task to automatically renew it ([Certbot](https://certbot.eff.org/) do both things).
* Watchdog module.

Please make sure you have sudo privileges before continuing.

## Downloading the script

Clone the repository with the following command:

```
git clone https://gitlab.com/guillearch/tomcat-ssl-auto-renewal.git
```

## Running the script

Run the script:

```
sudo python3 main.py <your-domain.com> &
```

Don't miss the ampersand (`&`) at the end of the command, as it puts the job in background! 

Please make sure you are using Python 3.6 or higher.