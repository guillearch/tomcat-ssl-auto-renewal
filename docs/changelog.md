# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- PyPi setup to install the application using pip.
- Option to run the application as a console script.
- Error handling.

## [0.1.0] - 2019-09-16

- First release! :tada: