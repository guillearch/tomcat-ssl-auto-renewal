import unittest
from main import is_domain


class MyTestCase(unittest.TestCase):
    def test_is_domain(self):
        self.assertTrue(is_domain('python.org'))
        self.assertTrue(is_domain('python.es'))
        self.assertTrue(is_domain('subdomain.python.org'))
        self.assertFalse(is_domain('python'))
        self.assertFalse(is_domain('python.o'))
        self.assertFalse(is_domain('https://python.org'))


if __name__ == '__main__':
    unittest.main()
