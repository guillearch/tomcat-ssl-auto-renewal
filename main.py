import os
import re
import subprocess
import sys
import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

pattern = re.compile(r'^([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,}$')
certificate = ['cert.pem', 'chain.pem', 'privkey.pem']


class DomainError(Exception):
    pass


class MyHandler(FileSystemEventHandler):
    def on_modified(self, event):
        os.chdir(path)
        for file in certificate:
            subprocess.call(['sudo', 'cp', file, '/opt/tomcat/conf'])
        subprocess.call(['sudo', 'systemctl', 'restart', 'tomcat'])


def is_domain(name):
    return bool(pattern.match(name))


if __name__ == "__main__":
    try:
        domain = sys.argv[1]
        path = f'/etc/letsencrypt/live/{domain}/'
        if is_domain(domain) is False:
            raise DomainError
        event_handler = MyHandler()
        observer = Observer()
        observer.schedule(event_handler, path=path, recursive=False)
        observer.start()
        while True:
            time.sleep(1)
        observer.join()
    except DomainError:
        print(f'Domain Error: "{domain}" is not a domain')
        exit()
    except InterruptedError:
        observer.stop()
    except OSError as e:
        print('OS Error: {0}'.format(e))
        exit()
    except KeyboardInterrupt:
        observer.stop()
